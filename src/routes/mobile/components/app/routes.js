let routes  = [
  {
    name: 'setting',
    path:'/setting',
    pageName: 'setting',
  },
    {
        name: 'about',
        path: '/about',
        pageName: 'about',
      },
      {
        name: 'home',
        path:'/home',
        pageName: 'home',
      }
];

exports.routes = routes;